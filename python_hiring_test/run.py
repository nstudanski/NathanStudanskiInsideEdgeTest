## Nathan Studanski
## nstudanski@yahoo.com
## 763-228-7398
## Written May 26, 2017
## For Inside Edge python test

"""Main script for generating output.csv."""
from natsort import natsorted

def main():
    ## Read in ./data/raw/pitchdata.csv and split into lines
	statLines = open("./data/raw/pitchdata.csv").read().split('\n')
	
	## Read in combinations.txt and split into lines
	##comboLines =  open("./data/reference/combinations.txt").read().split('\n')

	pitDic, hitDic, pitTeamDic, hitTeamDic = buildDic(statLines)
		
	## computeStatsCSVLines(dic, splits, subject)
	## Each of these will be a list of strings. Each such string is ready to be written as a line into the csv file
	pitStats = computeStatsCSVLines(pitDic, ['vs LHH', 'vs RHH'], 'PitcherId')
	hitStats = computeStatsCSVLines(hitDic, ['vs LHP', 'vs RHP'], 'HitterId')
	pitTeamStats = computeStatsCSVLines(pitTeamDic, ['vs LHH', 'vs RHH'], 'PitcherTeamId')
	hitTeamStats = computeStatsCSVLines(hitTeamDic, ['vs LHP', 'vs RHP'], 'HitterTeamId')
	
	## This works as sorted() normally except that it will sort such as 108, 109, 110, 1101111, 111, etc
	## https://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
	allLines = natsorted(pitStats + hitStats + pitTeamStats + hitTeamStats)

	writeTo = open("./data/processed/output.csv", 'w+')
	header = "SubjectId,Stat,Split,Subject,Value\n"
	writeTo.write(header)
	writeTo.writelines(allLines)
	
	writeTo.close()

## This is to read through the whole csv file and sum the stats
def buildDic(csvLines):
	'''
	Example of how I'm building the dictionary
	hitDic = {id: vs RHP: {stats}, vs LHP: {stats}}
	hitDic = { 123: R: {PA: 25, AB: 20, H: 5, TB: 12, BB + HBP: 2, SF: 0}, L: {PA...}}
	pitDic = {id: vs RHH: {stats}, vs LHH: {stats}}
	same for hitTeamDic, pitTeamDic
	[1], [2], [6], [7] respectively from the split lines and header
	'''	
	pitDic = {}
	hitDic = {}
	hitTeamDic = {}
	pitTeamDic = {}
	
	## This could be adapted for other statistics to become more general. Since I'm not doing SQL/panda method
	header = csvLines[0]
	## Drop the header line and the trailing blank line
	data = csvLines[1:-1]
	'''	
	[GameId', 'PitcherId', 'HitterId', 'PitcherSide', 'HitterSide', 'PrimaryEvent', 'PitcherTeamId', 'HitterTeamId', 'PA', 
			'AB', 'H', '2B', '3B', 'HR', 'TB', 'BB', 'SF', 'HBP'] format for data
	GameID 0, Primary Event, PA, 2B, 3B, HR aren't needed for the 4 stat calculations
	[1], [2], [6], [7] keys of respective dics dics
	[3], [4] are values of the above keys. So pitcherID 100 would be in pitcherDic and have values of vs RHH and vs LHH which each have the stat summations
	Avg = H/AB
	OBP = H + BB + HBP / AB + BB + SF + HBP
	SLG = TB/AB
	OPS = OBP + SLG
	PA >= 25 to count
	'''

	## Need to sum: PA, AB, H, TB, BB + HBP, SF
	## For cleanliness, BB + HBP will be bh
	for line in data:
		[_, pitID, hitID, pitSide, hitSide, _, pitTeamID, hitTeamID, paStr, abStr, hStr, _, _, _, tbStr, bbStr, sfStr, hbpStr] = line.split(',')
		## Only change is I'm adding bbStr and hbpStr to bh to save 3 additions
		pa, ab, h, tb, bh, sf = int(paStr), int(abStr), int(hStr), int(tbStr), int(bbStr) + int(hbpStr), int(sfStr)
		## These could be done by turning each column into a set and then you know each item already exists
		## However, given that I'm not using dataframes or tables, it's harder to get a column isolated, so this is linear time on the length of csv
		if pitID not in pitDic:
			pitDic[pitID] = {}
			pitDic[pitID]['R'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
			pitDic[pitID]['L'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
		if hitID not in hitDic:
			hitDic[hitID] = {}
			hitDic[hitID]['R'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
			hitDic[hitID]['L'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
		if pitTeamID not in pitTeamDic:
			pitTeamDic[pitTeamID] = {}
			pitTeamDic[pitTeamID]['R'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
			pitTeamDic[pitTeamID]['L'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
		if hitTeamID not in hitTeamDic:
			hitTeamDic[hitTeamID] = {}
			hitTeamDic[hitTeamID]['R'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}
			hitTeamDic[hitTeamID]['L'] = {'pa': 0, 'ab': 0, 'h': 0, 'tb': 0, 'bh': 0, 'sf': 0}

		## These are all essentially copies of each other
		pitDic[pitID][hitSide]['pa'] += pa
		pitDic[pitID][hitSide]['ab'] += ab
		pitDic[pitID][hitSide]['h'] += h
		pitDic[pitID][hitSide]['tb'] += tb
		pitDic[pitID][hitSide]['bh'] += bh
		pitDic[pitID][hitSide]['sf'] += sf

		hitDic[hitID][pitSide]['pa'] += pa
		hitDic[hitID][pitSide]['ab'] += ab
		hitDic[hitID][pitSide]['h'] += h
		hitDic[hitID][pitSide]['tb'] += tb
		hitDic[hitID][pitSide]['bh'] += bh
		hitDic[hitID][pitSide]['sf'] += sf

		pitTeamDic[pitTeamID][hitSide]['pa'] += pa
		pitTeamDic[pitTeamID][hitSide]['ab'] += ab
		pitTeamDic[pitTeamID][hitSide]['h'] += h
		pitTeamDic[pitTeamID][hitSide]['tb'] += tb
		pitTeamDic[pitTeamID][hitSide]['bh'] += bh
		pitTeamDic[pitTeamID][hitSide]['sf'] += sf

		hitTeamDic[hitTeamID][pitSide]['pa'] += pa
		hitTeamDic[hitTeamID][pitSide]['ab'] += ab
		hitTeamDic[hitTeamID][pitSide]['h'] += h
		hitTeamDic[hitTeamID][pitSide]['tb'] += tb
		hitTeamDic[hitTeamID][pitSide]['bh'] += bh
		hitTeamDic[hitTeamID][pitSide]['sf'] += sf

	## end for line in data
	return pitDic, hitDic, pitTeamDic, hitTeamDic

## The return will be a list of strings. Each string represents a CSV line for the output.csv: subjectId, stat, split, subject, value
## To accomodate this, splits will be passed in for ['vs LHP', vs 'RHP'] if it is a hitting dic and ['vs LHH', 'vs RHH'] for pitching. Subject will be a single one for the dictionary calling it: PitcherTeamId, HitterTeamId, HitterId, PitcherId
def computeStatsCSVLines(dic, splits, subject):
## z = format(x, '.3f')
	statLines = []
	for ID in dic.keys():

		## Build the stat strings for vs lefties for all 4 stats if the PA >= 25
		if(dic[ID]['L']['pa'] > 24):
			## This saves characters below. Contents of inner are {'pa': 52, 'h':25, ...}
			inner = dic[ID]['L']

			## Avg = H/AB
			avg = float(inner['h'])/inner['ab']
			## OBP = H + BB + HBP / AB + BB + SF + HBP
			obp = float((inner['h'] + inner['bh']))/(inner['ab'] + inner['bh'] + inner['sf'])
			## SLG = TB/AB
			slg = float(inner['tb'])/inner['ab']
			## OPS = OBP + SLG
			ops = obp + slg
			
			## split[0] will be either 'vs	LHP' or 'vs	LHH'
			avgLine = ID + ",AVG, " + splits[0] + "," + subject + "," + str(round(avg, 3)) + '\n'
			obpLine = ID + ",OBP, " + splits[0] + "," + subject + "," + str(round(obp, 3)) + '\n'
			slgLine = ID + ",SLG, " + splits[0] + "," + subject + "," + str(round(slg, 3)) + '\n'
			opsLine = ID + ",OPS, " + splits[0] + "," + subject + "," + str(round(ops, 3)) + '\n'
			
			statLines += [avgLine, obpLine, slgLine, opsLine]
			
		## Same as vs righties, only changes are the splits to [1] and the inner is the vs righties
		if(dic[ID]['R']['pa'] > 24):
			## This saves characters below. Contents of inner are {'pa': 52, 'h':25, ...}
			inner = dic[ID]['R']

			## Avg = H/AB
			avg = float(inner['h'])/inner['ab']
			## OBP = H + BB + HBP / AB + BB + SF + HBP
			obp = float((inner['h'] + inner['bh']))/(inner['ab'] + inner['bh'] + inner['sf'])
			## SLG = TB/AB
			slg = float(inner['tb'])/inner['ab']
			## OPS = OBP + SLG
			ops = obp + slg
			
			## split[0] will be either 'vs	LHP' or 'vs	LHH'
			avgLine = ID + ",AVG, " + splits[1] + "," + subject + "," + str(round(avg, 3)) + '\n'
			obpLine = ID + ",OBP, " + splits[1] + "," + subject + "," + str(round(obp, 3)) + '\n'
			slgLine = ID + ",SLG, " + splits[1] + "," + subject + "," + str(round(slg, 3)) + '\n'
			opsLine = ID + ",OPS, " + splits[1] + "," + subject + "," + str(round(ops, 3)) + '\n'
			
			statLines += [avgLine, obpLine, slgLine, opsLine]
			
	## end for ID in dic(keys)
	return statLines
if __name__ == '__main__':
    main()

